﻿Option Explicit On
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Xml
Imports System.Xml.Serialization
Imports System.IO

Public Class DataHandler
    '**********************************************************************************************************************************************************************
    ' This class handles all database functions for the application.
    '**********************************************************************************************************************************************************************
    Private cnSQLS As SqlConnection
    Private LogUtil As LogUtil

    Private mDBError As Boolean
    Public ReadOnly Property DBError As Boolean
        Get
            Return mDBError
        End Get
    End Property
    Private mDBErrorMsg As String
    Public ReadOnly Property DBErrorMsg As String
        Get
            Return mDBErrorMsg
        End Get
    End Property
    Private mAppQueueDT As DataTable

    Public Sub New()
        ' Any class instantiation processing is done here.
        Dim funcName As String = "DataHandler.New"
        mDBError = False
        mDBErrorMsg = ""
        cnSQLS = New SqlConnection
        If Not mDBError Then ' Connect to the DB on Nora.
            Try
                cnSQLS.ConnectionString = "Data Source=nora\SQLExpress; Initial Catalog=WengerUtilDB; User Id=sa; Password=wfm@sql12;"
                cnSQLS.Open()
            Catch ex As Exception
                mDBErrorMsg = "Error:" & funcName & ":Problem opening WengerUtilDB on nora\SQLExpress; message is:" & ex.Message
                mDBError = True
            End Try
        End If
        LogUtil = New LogUtil
        LogUtil.LoggingLevel = loggingLevel ' Public const from the main module.
        LogUtil.LogSource = AppName ' Public const from the main module.
    End Sub

    Public Sub ProcessAppQueue()
        ' This is the public method that is called to perform the app queue processing.  It does the following:
        ' - Reads the current contents of the AppQueue table in the WengerUtilDb into a working datatable.
        ' - Reads the datatable and submits each requested app in its own process thread.
        ' - As each app is submitted, deletes the request row from the AppQueue table (NOT the working datatable).
        ' - Each app submitted is logged to the event log.
        Dim funcName As String = "DataHandler.ProcessAppQueue"
        mDBError = False
        mDBErrorMsg = ""
        If Not mDBError Then ' Call private method to fill the datatable from the AppQueue table.
            FillAppQueueWorkTable()
            If mDBError Then
                Console.WriteLine(mDBErrorMsg)
                LogUtil.WriteLogFile(mDBErrorMsg, LogUtil.LogError)
            End If
        End If
        If Not mDBError Then ' Read the work datatable, execute each requested app, and delete the row from the physical table.
            For Each r As DataRow In mAppQueueDT.Rows
                ExecuteApp(r)
                If Not mDBError Then
                    Dim msgString As String = "Info:" & funcName & ":App successfully executed for " & r.Item("AppToRun").ToString.Trim & " " & r.Item("ParmList").ToString.Trim
                    Console.WriteLine(msgString)
                    LogUtil.WriteLogFile(msgString, LogUtil.LogInformation)
                Else
                    Console.WriteLine(mDBErrorMsg)
                    LogUtil.WriteLogFile(mDBErrorMsg, LogUtil.LogError)
                End If
                ' We will continue to attempt to execute all app queue entries, even if one fails. We just don't delete the failing one.
                If Not mDBError Then ' Delete the AppQueue table entry for a successful app execution.
                    PurgeAppQueueRow(r)
                    If mDBError Then ' Problem deleting AppQueue record; it may be executed again on the next pass.
                        Console.WriteLine(mDBErrorMsg)
                        LogUtil.WriteLogFile(mDBErrorMsg, LogUtil.LogError)
                        ' We don't end the For loop, but instead continue to try to execute any other application requests.
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub FillAppQueueWorkTable()
        ' This private method retrieves all the data currently in the AppQueue table in the WengerUtilDB database on Nora and fills
        ' the working datatable with it.
        Dim funcName As String = "DataHandler.FillAppQueueWorkTable"
        mDBError = False
        mDBErrorMsg = ""
        Dim sqlString As String = ""
        Dim da As SqlDataAdapter = Nothing
        mAppQueueDT = New DataTable
        sqlString = "select "
        sqlString &= "RecordId, AppToRun, ParmList, SubmittingUser "
        sqlString &= "from AppQueue "
        Try
            da = New SqlDataAdapter(sqlString, cnSQLS)
            da.Fill(mAppQueueDT)
            mAppQueueDT.TableName = "AppQueue"
            LogUtil.WriteLogFile("Info:" & funcName & ":AppQueue table data successfully retrieved from WengerUtilDB on Nora.")
        Catch ex As Exception
            mDBErrorMsg = "Error:" & funcName & ":Error retrieving AppQueu table data from WengerUtilDB on Nora. SQL is: " & sqlString & ":Error is: " & ex.Message
            LogUtil.WriteLogFile(mDBErrorMsg)
            mDBError = True
        End Try
    End Sub

    Private Sub ExecuteApp(ByVal r As DataRow)
        ' This private method executes the application described in the datarow passed in.
        Dim funcName As String = "DataHandler.ExecuteApp"
        mDBError = False
        mDBErrorMsg = ""
        Dim wrkMsg As String = ""
        Dim startInfo As New ProcessStartInfo
        Dim appName As String = r.Item("AppToRun").ToString.Trim
        Dim appParms As String = r.Item("ParmList").ToString.Trim
        wrkMsg = "Executing app / parms: " & appName.Trim & " / " & appParms.Trim
        LogUtil.WriteLogFile(wrkMsg)
        startInfo.FileName = appName
        startInfo.Arguments = appParms
        startInfo.WorkingDirectory = Path.GetDirectoryName(appName)
        'startInfo.UseShellExecute = True
        Try
            Process.Start(startInfo)
        Catch ex As Exception
            mDBErrorMsg = "Error:" & funcName & ":Error starting " & appName.Trim & " with parms " & appParms.Trim & "; Error is: " & ex.Message
            LogUtil.WriteLogFile(mDBErrorMsg)
            mDBError = True
        End Try
    End Sub

    Private Sub PurgeAppQueueRow(ByVal r As DataRow)
        ' This private method deletes the AppQueue row based on the datarow passed in.
        Dim funcName As String = "DataHandler.PurgeAppQueueRow"
        mDBError = False
        mDBErrorMsg = ""
        Dim cmdSQLS As New SqlCommand
        Dim sqlString As String = ""
        sqlString = "delete from AppQueue "
        sqlString &= "where "
        sqlString &= "RecordId=" & r.Item("RecordId").ToString.Trim & " "
        Try
            cmdSQLS.CommandText = sqlString
            cmdSQLS.CommandTimeout = 0
            cmdSQLS.Connection = cnSQLS
            cmdSQLS.ExecuteNonQuery()
        Catch ex As Exception
            mDBErrorMsg = "Error:" & funcName & ":Problem deleting AppQueue data row for record " & r.Item("RecordId").ToString & "; error is: " & ex.Message
            LogUtil.WriteLogFile(mDBErrorMsg)
            mDBError = True
        End Try
    End Sub

End Class
