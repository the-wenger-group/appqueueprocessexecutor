﻿Option Explicit On
Imports System
Imports System.IO
Imports System.Text
Imports System.Data
Imports System.Diagnostics

' *********************************************************************************************************************************************************************
' This class handles all logging within the application. Log files are located in the same folder as the application. If the log file does not exist, it is created
' with the first log message that is written. This class also checks the log file size, and if it is too big, it renames it for archiving purposes.
' *** This is a BASE class, which does / should NOT instantiate any other classes created in this application.
'
' NOTES
' - A default log file name (log.txt) is provided.
' - A check against the current log.txt file size is done each time the class is instantiated; if the file size is larger than a certain size (as set by a constant in this
'   class,) the current log.txt file is renamed.  The next write against the log file will automatically create a new file if it does not already exist.
' - Archived log files are located in the same folder (usually the app folder) as the log.txt file; they are renamed as log_old_yyyymmddhhmmss.txt.
' - When this class is first instantiated in an app, it checks the number of archived log files, and if there are more of them than allowed (the number is set by a 
'   constant in this class) then older files are purged. This keeps the number of log files down.
' *********************************************************************************************************************************************************************
Public Class LogUtil
    Private Const logFileSize As Integer = 4000000 ' Max log files size.
    Private Const numArchivedLogFiles As Integer = 10 ' Max number of archived log files to keep.
    Private Const logFileName As String = "log.txt" ' Default current log file name.
    ' The message logging level can be set or returned via this property once the class is instantiated. Log messages passed in with a value less than or equal
    ' to the LoggingLevel property will be written to the log file. The logType parameter (which defaults to 1) is used to determine the level of the message being
    ' passed to the WriteLogFile routine and whether it will be written to the log file or not.
    ' A logging level of zero is defined as no messages will be logged.
    ' This property can be set by the calling application as needed. For instance, testing log messages that are part of development can be written with a logType of 2,
    ' and when the app is put into production, the app can set the LoggingLevel back to 1 in order to ignore those messages.
    Private mLoggingLevel As Integer
    Public Property LoggingLevel As Integer
        Get
            Return mLoggingLevel
        End Get
        Set(value As Integer)
            mLoggingLevel = value
        End Set
    End Property
    Private mLogSource As String ' This will contain the application name for the logging source.
    Public Property LogSource As String
        Get
            Return mLogSource
        End Get
        Set(value As String)
            mLogSource = value
        End Set
    End Property
    ' These three constants are used as properties to enable setting the WriteLog severity values by calling routines.
    Private Const mLogInformation As Integer = 1
    Public ReadOnly Property LogInformation As Integer
        Get
            Return mLogInformation
        End Get
    End Property
    Private Const mLogWarning As Integer = 2
    Public ReadOnly Property LogWarning As Integer
        Get
            Return mLogWarning
        End Get
    End Property
    Private Const mLogError As Integer = 3
    Public ReadOnly Property LogError As Integer
        Get
            Return mLogError
        End Get
    End Property

    Public Sub New()
        ' This routine performs initialization tasks whenever the class is instantiated. Each of the tasks is commented in the code that makes up this routine.
        mLoggingLevel = 1 ' Default logging level.
        ' A default application log source name is set here. The calling routine that consumes this app MUST set the application name (which should be made available
        ' via a global constant or something) in this property before writing any logs. In addition, the registry must contain this application name as a key for the
        ' Application event log, which must be manually set up via regedit. See the comments in the WriteLog method for more info.
        mLogSource = "WengerApplication"
        ' Check the current log file size and rename if larger than the threshold.
        CheckLogFileSize()
        ' Check the number of archived files in the folder and delete older files beyond the value in the numArchivedLogFiles constant.
        CleanUpArchivedFiles()
    End Sub

    Public Sub WriteLogFile(ByVal logText As String, Optional ByVal logType As Integer = 1)
        ' **** NOTE - this method is being deprecated, in favor of writing directly to the system event logs using the WriteLog method in this class. This has
        '             been set to Private; if it's needed for some reason, you can make it public again.
        ' This routine writes the log file message to the log file. It first checks the logType passed in (default 1 if no parameter) against the mLoggingLevel
        ' property, and only writes the message if the logType is less than or equal to that logging level. If the logging level is zero, no message is written,
        ' even in if the logType passed in is zero.
        ' A time stamp and the user running the program are prepended to the log file line before the message itself is written.
        If mLoggingLevel > 0 Then
            If logType <= mLoggingLevel Then ' Valid message found to write, based on logging level.
                ' We enclose the entire process in a Try/Catch; if there is an error, we just won't get a log file message. Since we don't know what kind of
                ' app is calling this class (console, forms, etc.), we try a number of different ways to write the message out (each in their own Try/Catch) so
                ' that we get some kind of indication that there's a problem - since the logging isn't working.
                Try
                    Dim formatLine As String = System.DateTime.Now & ":" & Environment.UserName.ToString.ToUpper
                    Dim logWriter As New StreamWriter(logFileName, True) ' Always append to the log file.
                    formatLine = formatLine & ":" & logText.Trim
                    'Console.WriteLine(formatLine)
                    logWriter.WriteLine(formatLine)
                    logWriter.Close()
                Catch ex As Exception
                    Try
                        Console.WriteLine("Error writing log file; contact IT!" & vbCrLf & ex.Message)
                    Catch ex2 As Exception
                    End Try
                End Try
            End If
        End If
    End Sub

    Public Sub WriteLog(ByVal logText As String, Optional ByVal severityParm As Integer = 1, Optional ByVal logType As Integer = 1)
        ' This routine writes the log file message to the system event log on the machine this app is running on. It's an alternate - and preferred way - of 
        ' logging events for the application.
        ' The routine writes the log message only if the logType passed in is less than or equal to the mLoggingLevel property, which is set to 1 by default.
        ' This allows the calling app to set testing to 2 or higher and log test messages, and then for production to set the logging lower and eliminate the
        ' test messages. If the LoggingLevel property has been set to zero (0), the NO messages are logged.
        ' The severityParm is optional, and defaults to 1 (informational) if not supplied. The following values are available:
        ' - 1 - info
        ' - 2 - warning
        ' - 3 - error
        ' IMPORTANT NOTE - in order for this routine to work, the logSource must already have been set up in the Application event log. There is code included
        '                  to try to create it, but it will not work due to permission issues. Hence, it is best just to set up the application source in the
        '                  registry of the machine that it's going to run on using Regedit. The new key (such as CVFFFarmStatusImportUpload) that is the
        '                  application name is added to the HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Eventlog\Application key in the registry; the
        '                  default subkey, which is created automatically, can be left blank. The entry just needs to exist in order for this routine's WriteEntry
        '                  call to work. The test for the source existing has been left in here, though it will generate an error if the app has not been set up.
        If mLoggingLevel > 0 Then
            If logType <= mLoggingLevel Then ' Message valid to be logged.
                ' The entire process is enclosed in a Try/Catch so that we don't blow up any calling routine if there's a logging error. In cases of an
                ' error, we simply try to notify several different ways, based on a console write or a message box in hopes that someone will catch it.
                Try
                    Dim logSource = mLogSource
                    Dim logLog As String = "Application"
                    Dim logEvent As String = logText
                    Dim logMachine As String = "."
                    Dim logData As New EventSourceCreationData(logSource, logLog)
                    Try
                        If Not EventLog.SourceExists(logSource, logMachine) Then
                            EventLog.CreateEventSource(logData)
                        End If
                    Catch exa As Exception
                        Try
                            MsgBox("The application name " & logSource & " has not been set up as an event log source, " &
                                   "which must be done manually via regedit. See code notes for more info.")
                        Catch ex As Exception
                        End Try
                    End Try
                    Dim eLog As New EventLog(logLog, logMachine, logSource)
                    Select Case severityParm
                        Case 1
                            eLog.WriteEntry(logEvent, EventLogEntryType.Information)
                        Case 2
                            eLog.WriteEntry(logEvent, EventLogEntryType.Warning)
                        Case 3
                            eLog.WriteEntry(logEvent, EventLogEntryType.Error)
                    End Select
                Catch ex As Exception
                    Try
                        MsgBox("Error writing event log; contact IT!" & vbCrLf & ex.Message)
                    Catch ex1 As Exception
                    End Try
                    Try
                        Console.WriteLine("Error writing event log; contact IT!" & vbCrLf & ex.Message)
                    Catch ex2 As Exception
                    End Try
                End Try
            End If
        End If
    End Sub

    Private Sub CheckLogFileSize()
        ' This routine checks the size of the current log file (as specified by the logFileName constant) and if it is larger than the size
        ' specified by the logFileSize constant, renames it in order to archive it. The try/catch blocks are used to make sure the logging function
        ' does not terminate the app, and that we get some kind of notification that there was a log file archiving problem.
        Try
            If File.Exists(logFileName) Then
                Dim logInfo As New FileInfo(logFileName)
                Dim logLength As Integer = logInfo.Length
                Dim newLogName As String = "log_old_" & Date.Today.ToString("yyyyMMdd_") & TimeOfDay.ToString("hhmmss") & ".txt"
                If logLength > logFileSize Then ' Rename the existing log file to archive it.
                    My.Computer.FileSystem.RenameFile(logFileName, newLogName)
                End If
            End If
        Catch ex As Exception
            Try
                Console.WriteLine("Error archiving log file; contact IT!" & vbCrLf & ex.Message)
            Catch ex2 As Exception
            End Try
        End Try
    End Sub

    Private Sub CleanUpArchivedFiles()
        ' This routine retrieves the list of archived log files in the folder, and deletes the older ones beyond the number allowed as specified by the numArchivedLogFiles
        ' constant.
        Try
            Dim di As New IO.DirectoryInfo(".")
            Dim fList As IO.FileInfo() = di.GetFiles("log_old*.txt")
            If fList.Count > numArchivedLogFiles Then ' Number of files greater than the number
                Dim query = (From e As IO.FileInfo In fList Order By e.Name.ToString)
                Dim purgeCt As Integer = 0
                For Each e1 As IO.FileInfo In query
                    My.Computer.FileSystem.DeleteFile(e1.Name)
                    purgeCt += 1
                    If purgeCt >= (fList.Count - numArchivedLogFiles) Then
                        Exit For
                    End If
                Next
            End If
        Catch ex As Exception
        End Try
    End Sub

End Class
