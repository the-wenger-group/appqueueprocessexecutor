﻿Option Explicit On
Imports System
Imports System.IO
Imports System.Text
Imports System.Data

Module Module1
    '**********************************************************************************************************************
    ' Program Notes and Comments
    '
    ' 10/15/2012 jdw - Initial coding.
    '
    ' This application reads the AppQueue table in the WengerUtilDB on Nora and submits any processes for execution that have
    ' been requested in that table. It then deletes the rows from the AppQueue table that had processes submitted.
    ' 
    ' NOTES
    ' - This application was developed as an intermediate step for executing requested applications from web pages. Rather than
    '   execute the app directly from the web page, which not only raises security issues, but is technically more complex, web
    '   pages write the information for the app to be executed to the AppQueue table, and this program then reads that table and
    '   executes the apps requested.
    ' - This application was designed as a console app that is scheduled periodically to run from the Windows scheduler on Nora.
    '   The execution schedule is determined by how often we want this to run and look for apps to execute. Initially (October 2012),
    '   this was set up to run every minute.
    ' - The event log source MUST be set up on the machines this app runs on, in order for logging to execute properly.  Run the .reg
    '   file found in the same folder as the project .sln file on the machines this needs to run on.
    ' - In order for this application to run properly, the group policy on the machine it is running on must be modified so as to
    '   avoid "Publisher could not be verified" warning boxes. If this box comes up when the app runs as scheduled, it will just
    '   "hang" waiting for a reply to the box. Since it's running in the background, the box can't be replied to. To set the group
    '   policy appropriately on the machine this app runs on, do the following:
    '   - From Start/Run, type gpedit.msc and press Enter
    '   - Go to Local Computer Policy/User Configuration/Administrative Templates/Windows Components/Attachment Manager
    '   - In the Inclusion list for moderate risk file types rule, add '.exe' extensions to it (you made need to enable the rule).
    '   - This policy will then apply to ALL users on the machine.
    '
    ' MODIFICATIONS
    ' 02/09/18 jdw - Modified to eliminate use of Windows Event Log, and instead log messages to a log file with the app.
    '
    '**********************************************************************************************************************

    ' Global variables.

    Dim programVersion As String = "Version:20180209a" ' Change this version identifier whenever a modification is made to the application.

    Public Const loggingLevel As Integer = 1 ' Set this to 2 for development/testing, 1 for production.
    Public Const AppName As String = "AppQueueProcessExecutor" ' For use as needed in the code.
    Public DataHandler As DataHandler ' Handles all of the database access for the app.
    Public LogUtil As LogUtil ' Handles all logging for the app.

    Sub Main()
        Dim appError As Boolean = False
        Console.WriteLine("App Queue Process Executor " & programVersion.Trim)
        AppInit()
        If DataHandler.DBError Then ' Instantiation error.
            appError = True
        End If
        If Not appError Then ' Log the program start info.
            LogUtil.WriteLogFile("AppQueueProcessExecutor " & programVersion.Trim & " started.", LogUtil.LogInformation)
        End If
        If Not appError Then ' Execute the method to handle the AppQueue processing.
            DataHandler.ProcessAppQueue()
        End If
        Console.WriteLine("Press any key to end the program...")
        'Console.ReadKey()
    End Sub

    Sub AppInit()
        ' This routine performance initialization functions for the app, including instantiating classes.
        LogUtil = New LogUtil
        LogUtil.LoggingLevel = loggingLevel
        LogUtil.LogSource = AppName
        LogUtil.WriteLogFile("LogUtil Instantiation Completed")
        DataHandler = New DataHandler
        If DataHandler.DBError Then ' Whoops...
            LogUtil.WriteLogFile("Error:DataHandlerClassInstatiationError:" & DataHandler.DBErrorMsg.Trim, LogUtil.LogError)
            Console.WriteLine("Error:DataHandlerClassInstatiationError:" & DataHandler.DBErrorMsg.Trim)
        Else
            LogUtil.WriteLogFile("DataHandler Instantiation Completed")
        End If
    End Sub

End Module
